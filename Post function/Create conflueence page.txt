import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.jira.issue.Issue
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

/**
 * Retrieve the primary confluence application link
 * @return confluence app link
 */
def ApplicationLink getPrimaryConfluenceLink() {
    def applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService.class)
    final ApplicationLink conflLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class)
    conflLink
}

def confluenceLink = getPrimaryConfluenceLink()
assert confluenceLink // must have a working app link set up

def authenticatedRequestFactory = confluenceLink.createImpersonatingAuthenticatedRequestFactory()

// set the page title - this should be unique in the space or page creation will fail
def pageTitle = issue.key + " Functional Requirements Specification"
def pageBody = """
h1. 相關SR需求單：
http://192.168.49.134:8080/browse/${issue.key}

h1. 1.	Introduction
h2. 1.1	Purpose (說明此份FRS目的)

h2. 1.2	Objectives (說明此業務的目標)

h2. 1.3	Scope (需求包含範圍list)

h2. 1.4	Out of Scope (需求不包含範圍list)
<Provide a list of features and functions that are out of scope>

h2. 1.5	Assumptions, Dependencies, Constraints (若為專案需求，則於專案相關文件中紀錄及追蹤)
||Type||Description||Associated Risks||Mitigation||
|<Assumption>|<Description>|<Risk>|<Mitigation>|
|<Dependency>|<Description>|<Risk>|<Mitigation>|
|<Constraint>|<Description>|<Risk>|<Mitigation>|

h1. 2.	Functional Requirements 

h2. 2.1	System Requirements

h3. 2.1.1	System Functionality Requirements
||BR No ||Requirement ID||Requirement Description||
|<ID>| | |
|<ID>| | |
|<ID>| | |

- FR-0001_xxxxx
- FR-0002_xxxxx
- FR-0003_xxxxx
- FR-0004_xxxxx

System Interface Requirements：
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.1.2	ePolicy Requirements
Please list the NEW ePolicy requirement in the below table if any :
||Requirement ID||BR No||ePolicy Sample attachment(With Sample data)||ePolicy Template attachment(Indicate the Variable section)||Requirement Description||
|<ID>| | | |<Requirement Description>|
|<ID>| | | |<Requirement Description>|
|<ID>| | | |<Requirement description>|

h2. 2.2	Reporting Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h2. 2.3	Data Requirements
h3. 2.3.1	Data Access Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.3.2	Data Import/Export Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.3.3	Data Migration/Conversion Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.3.4	Data Backup, Restore, and Archiving Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|


h2. 2.4	Non-Functional Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|



h2. 2.5	Security Requirements

h3. 2.5.1	User Role/Authorization Control Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.5.2	User Administration Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.5.3	System Administration Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.5.4	Network Security Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.5.5	Physical Security Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|

h3. 2.5.6	User ID Requirements
||Requirement ID||BR No||Requirement Description||
|<ID>| |<Requirement Description>|
|<ID>| | |
|<ID>| |<Requirement description>|



h1. 3.	Glossary
||Abbreviation, Acronym, and Term||Definition||
| | |
| | |
| | |

h1. 4.	Reference Documents
||Document Number||Document Title||Document Type||
|<Document Number>|<Document Title>|<Document Type>|
|<Document Number>|<Document Title>|<Document Type>|
|<Document Number>|<Document Title>|<Document Type>|

h1. 5.	Attachments/Appendix


h1. 6.	Signatures
By signing this section, the individuals listed below acknowledge that they have reviewed and approved the scope of the effort described in this Functional Requirements Specification (FRS) for *<Project Name/Application Name/System Name>.*  The signatures below represent the approval for execution of this FRS.

h1. 7.	Distribution List
This document shall be made available as soft copies to the following users below: 
||Name||Designation/Role||Department||
| | | |
| | | |
| | | |
"""

def params = [
    type : "page",
    title: pageTitle,
    space: [
        key: "PCAL" // set the space key - or calculate it from the project or something
    ],
     // if you want to specify create the page under another, do it like this:
     ancestors: [
         [
             type: "page",
             id: "16842756",
         ]
     ],
    body : [
        storage: [
            value         : pageBody,
            representation: "wiki"
        ],
    ],
]

authenticatedRequestFactory
    .createRequest(Request.MethodType.POST, "rest/api/content")
    .addHeader("Content-Type", "application/json")
    .setRequestBody(new JsonBuilder(params).toString())
    .execute(new ResponseHandler<Response>() {
        @Override
        void handle(Response response) throws ResponseException {
            if (response.statusCode != HttpURLConnection.HTTP_OK) {
                throw new Exception(response.getResponseBodyAsString())
            } else {
                def webUrl = new JsonSlurper().parseText(response.responseBodyAsString)["_links"]["webui"]
            }
        }
    })