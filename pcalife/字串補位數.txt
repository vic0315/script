import groovy.json.JsonSlurper;
import groovy.json.StreamingJsonBuilder;
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.config.ConstantsManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption
import com.onresolve.scriptrunner.db.DatabaseUtil
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.util.UserMessageUtil
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.transform.BaseScript
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.apache.http.HttpRequestInterceptor
import org.apache.http.HttpRequest
import org.apache.http.protocol.HttpContext
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response
import com.atlassian.jira.util.json.JSONObject
import java.nio.charset.StandardCharsets
import groovy.sql.Sql
import java.sql.SQLException
import java.sql.SQLIntegrityConstraintViolationException
import org.springframework.dao.DuplicateKeyException


def commentManager = ComponentAccessor.getCommentManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

def issuem = ComponentAccessor.getIssueManager()
def issue = issuem.getIssueByCurrentKey("SDLCPROM-1")
def cfm = ComponentAccessor.getCustomFieldManager()
def cf = cfm.getCustomFieldObjectsByName("Promote程式碼清單")
String cfv = issue.getCustomFieldValue(cf[0])

def now = new Timestamp((new Date() + 7).time)


def KEY = issue.key.split("\\-")
log.warn(KEY[1].toString())
String KEYv = String.format("%05d", KEY[1].toInteger())

def linkissuem = ComponentAccessor.getCustomFieldManager()
def linkissue = cfm.getCustomFieldObjectsByName("佈署案件單")
//String linkissuev = issue.getCustomFieldValue(linkissue[0])
//String linkissuevR = linkissuev[1..-2]

String linkissuetest = "S40000"
//log.warn(linkissuevR)

def ENVM = ComponentAccessor.getCustomFieldManager()
def ENV = cfm.getCustomFieldObjectsByName("執行環境(參數)")

String ENVvs = ""
String ENVv = issue.getCustomFieldValue(ENV[0])

def paser = new JsonSlurper()
def infosize = paser.parseText(cfv)
log.warn"$infosize"
//return infosize.values()[0].getAt('orderPosition').size()

int F1 = infosize.values()[0].getAt('orderPosition').size()

String Reporter = issue.reporter as ApplicationUser

def jsonBody = [:]

for(int i = 0;i<F1;i++){
int rowID = infosize.values()[0].getAt('id')[i]
int F1K = infosize.values()[0].getAt('orderPosition')[i]+1
String F1KF = String.format("%02d", F1K)
String NAME = infosize.values()[0].getAt('field-1')[i] //程式物件名稱
def F2ID = infosize.values()[0].getAt('field-2')[i] //動作類型ID
String F2 = infosize.values()[0].getAt('field-2-name')[i] //動作類型
String F2R = F2[4..-3]
String DESCRIPTION = infosize.values()[0].getAt('field-3')[i] //程式物件描述
String CHECK = infosize.values()[0].getAt('field-4')[i] //完成否ID
String CHECKR = CHECK[1..-2]
def TYPEID = infosize.values()[0].getAt('field-6')[i] //程式物件類型ID
String TYPE = infosize.values()[0].getAt('field-6-name')[i]
String TYPER = TYPE[1..-2] //程式物件類型名稱

def baseURL = "https://twlifejirauat.pru.intranet.asia:8443/rest/stj/1.0/table/data/" + rowID
log.warn('INSERT INTO PUWCOM.WAPHPF(SERIAL_NO,NOTREF,WORKU,UENV,OBJLEVL,ZINPUSR,PROCFLG,PRTRANNO,USER_PROFILE,JOB_NAME,DATIME) VALUES (\'JIRAUAT\',\'NT0'+KEYv+F1KF+'\''+','+'\''+linkissuetest+'\','+'\'LAS\','+'\''+ENVvs+'\','+'\''+issue.reporter.id+'\','+'\'0\','+'0,'+'\'\',\'\',\''+now+'\')')
log.warn('INSERT INTO PUWCOM.WAPDPF(SERIAL_NO,NOTREF,OBJECT_NAME,OBJECT_TYPE,DESCRIPTION,USER_PROFILE,JOB_NAME,DATIME,ACTN) VALUES (\'JIRAUAT\',\'NT0'+KEYv+F1KF+'\''+',\''+NAME+'\','+'\''+TYPER+'\',\''+DESCRIPTION+'\','+'\'\',\'\',\''+now+'\',\''+F2R+'\')')

}
