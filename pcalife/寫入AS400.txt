import groovy.json.JsonSlurper;
import groovy.json.StreamingJsonBuilder;
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.bc.issue.search.SearchService 
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.config.ConstantsManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.onresolve.scriptrunner.db.DatabaseUtil
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.util.UserMessageUtil

def commentManager = ComponentAccessor.getCommentManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

def issuem = ComponentAccessor.getIssueManager()
//def issue = issuem.getIssueByCurrentKey("SDLC-175")
def cfm = ComponentAccessor.getCustomFieldManager()
def cf = cfm.getCustomFieldObjectsByName("Promote程式碼清單")
String cfv = issue.getCustomFieldValue(cf[0])

def now = new Timestamp((new Date() + 7).time)

def linkissuem = ComponentAccessor.getCustomFieldManager()
def linkissue = cfm.getCustomFieldObjectsByName("佈署案件單")
String linkissuev = issue.getCustomFieldValue(linkissue[0])
String linkissuevR = linkissuev[1..-2]

String linkissuetest = "S40000"
//log.warn(linkissuevR)

def ENVM =  ComponentAccessor.getCustomFieldManager()
def ENV = cfm.getCustomFieldObjectsByName("執行環境(參數)")
String ENVv = issue.getCustomFieldValue(ENV[0])

def paser = new JsonSlurper()
def infosize = paser.parseText(cfv)
log.warn"$infosize"
//return infosize.values()[0].getAt('orderPosition').size()

int F1 = infosize.values()[0].getAt('orderPosition').size()

String Reporter = issue.reporter as ApplicationUser
//return infosize.values()[0].getAt('field-1')
//return infosize.values().getAt('orderPosition').size()

for(int i = 0;i<F1;i++){
    int F1K = infosize.values()[0].getAt('orderPosition')[i]+1
	String F2 = infosize.values()[0].getAt('field-2-name')[i]
    String F2R = F2[4..-3]
	String F3 = infosize.values()[0].getAt('field-4-name')[i]
    String F3R = F3[1..-2]
	String NAME = infosize.values()[0].getAt('field-1')[i]
	String DESCRIPTION = infosize.values()[0].getAt('field-3')[i]
	String F6 = infosize.values()[0].getAt('field-5')[i]
	String TYPE = infosize.values()[0].getAt('field-6-name')[i]
   	String TYPER = TYPE[1..-2]
    
    //String cf2valuedesc = "Promote程式碼清單: \n ${Sourcevalue1.toString().replace(',','\n').replace('[','').replace(']','')}"
    //log.debug"${cf2valuedesc}"
    
    	//log.warn(F2R+" "+F3R+" "+NAME+" "+DESCRIPTION+" "+F6)
//        log.warn('INSERT INTO WAPHPF(SERIAL_NO,NOTREF,WORKU,UENV,OBJLEVL,ZINPUSR,PROCFLG,PRTRANNO,USER_PROFILE,JOB_NAME,DATIME) VALUES (\'JIRAUAT\',\''+issue.key+'-'+F1K+'\''+','+'\'S40000\','+'\'LAS\','+'\'PDEV\','+'\''+Reporter+'\','+F3R+','+NAME+','+DESCRIPTION+','+F6+');')
        log.warn('INSERT INTO PUWCOM.WAPHPF(SERIAL_NO,NOTREF,WORKU,UENV,OBJLEVL,ZINPUSR,PROCFLG,PRTRANNO,USER_PROFILE,JOB_NAME,DATIME) VALUES (\'JIRAUAT\',\''+issue.key+'-'+F1K+'\''+','+'\''+linkissuetest+'\','+'\'LAS\','+'\''+ENVv+'\','+'\''+'pcadmin'+'\','+'\'0\','+'0,'+'\'\',\'\',\''+now+'\')')
        log.warn('INSERT INTO PUWCOM.WAPDPF(SERIAL_NO,NOTREF,OBJECT_NAME,OBJECT_TYPE,DESCRIPTION,USER_PROFILE,JOB_NAME,DATIME,ACTN) VALUES (\'JIRAUAT\',\''+issue.key+'-'+F1K+'\''+',\''+NAME+'\','+'\''+TYPER+'\',\''+DESCRIPTION+'\','+'\'\',\'\',\''+now+'\',\''+F2R+'\')')

    //return F1+" "+F2+" "+F3+" "+F4+" "+F5+" "+F6
    
def nProjects = DatabaseUtil.withSql('AS400DB') { sql ->

def SQL1 = sql.execute('INSERT INTO PUWCOM.WAPHPF(SERIAL_NO,NOTREF,WORKU,UENV,OBJLEVL,ZINPUSR,PROCFLG,PRTRANNO,USER_PROFILE,JOB_NAME,DATIME) VALUES (\'JIRAUAT\',\''+issue.key+'-'+F1K+'\''+','+'\''+linkissuetest+'\','+'\'LAS\','+'\''+ENVv+'\','+'\''+'pcadmin'+'\','+'\'0\','+'0,'+'\'\',\'\',\''+now+'\')')
def SQL2 = sql.execute('INSERT INTO PUWCOM.WAPDPF(SERIAL_NO,NOTREF,OBJECT_NAME,OBJECT_TYPE,DESCRIPTION,USER_PROFILE,JOB_NAME,DATIME,ACTN) VALUES (\'JIRAUAT\',\''+issue.key+'-'+F1K+'\''+',\''+NAME+'\','+'\''+TYPER+'\',\''+DESCRIPTION+'\','+'\'\',\'\',\''+now+'\',\''+F2R+'\')')

   	try {
    	assert SQL1 : "寫入PUWCOM.WAPHPF"
         log.warn("寫入PUWCOM.WAPHPF成功")
        UserMessageUtil.success('AS400寫入成功')   
    	commentManager.create(issue,issue.assignee,'第'+F1K+'列程式碼寫入PUWCOM.WAPHPF成功',false)
	} catch (AssertionError e) {
         log.warn("寫入PUWCOM.WAPHPF失敗")
        UserMessageUtil.success('AS400寫入失敗')   
    	commentManager.create(issue,issue.assignee,'第'+F1K+'寫入PUWCOM.WAPHPF失敗',false)
    }

       	try {
    	assert SQL1 : "寫入PUWCOM.WAPDPF"
         log.warn("寫入PUWCOM.WAPDPF成功")
        UserMessageUtil.success('AS400寫入成功')   
    	commentManager.create(issue,issue.assignee,'第'+F1K+'寫入PUWCOM.WAPDPF成功',false)
	} catch (AssertionError e) {
         log.warn("寫入PUWCOM.WAPDPF失敗")
        UserMessageUtil.success('AS400寫入失敗')   
    	commentManager.create(issue,issue.assignee,'第'+F1K+'寫入PUWCOM.WAPDPF失敗',false)
    }
    
        //UserMessageUtil.success('AS400寫入成功')
    
    //commentManager.create(issue,issue.assignee,'INSERT INTO PUWCOM.WAPDPF(SERIAL_NO,NOTREF,OBJECT_NAME,OBJECT_TYPE,DESCRIPTION,USER_PROFILE,JOB_NAME,DATIME,ACTN) VALUES (\'JIRAUAT\',\''+issue.key+'-'+F1K+'\''+',\''+NAME+'\','+'\''+TYPER+'\',\''+DESCRIPTION+'\','+'\'\',\'\',\''+now+'\',\''+F2R+'\')'
//,false)
    //log.warn(sql.firstRow('select SERIAL_NO,NOTREF,WORKU,UENV,OBJLEVL,ZINPUSR,PROCFLG,PRTRANNO,USER_PROFILE,JOB_NAME,DATIME from PUWCOM.WAPHPF'))
}
}