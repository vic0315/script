if(issue.summary == '規劃/設計/預算'){
     return true
}
else if(issue.summary == '請購/發包申請'){
     return true
}
else{
    if(issue.getAttachments().find{it.filename.endsWith(".pdf")}){
          return true   
    }
    else if(issue.getAttachments().find{it.filename.endsWith(".doc")}){
          return true   
    }
    else if(issue.getAttachments().find{it.filename.endsWith(".docx")}){
          return true   
    }
    else if(issue.getAttachments().find{it.filename.endsWith(".jpg")}){
          return true  
    }
    else{
          return false 
    }   
}
